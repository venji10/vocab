/*
 *   main.c: The main file of vocab
 *
 *   Copyright (C) 2021 @venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "main.h"

int main (int argc, char **argv)
{

    if(argc != 2) 
    {
        printf("not exactly one argument provided!\n");
        return 1;
    }

    FILE *fp;
    if(!(fp = fopen(argv[1], "r")))
    {
        printf("fopen error: File not found / no permissions?\n");
        return 1;
    }

    init_sections(fp);

    section_count = get_section_count();

    while(choose_section()) { //continue asking for correct section number, if previously entered number isn't a section id
        printf("Sorry, try that again!\n");
    }

    getchar(); // getchar() because there is still one in "queue"

    // perma loop random questions
    choose_random_questions(sel_section);

    return 0;

}

int init_sections (FILE *fp) // TODO: FIX LAST ANSWER BEING PRINTED TWO TIMES
{
    int curr_char_type = CHARTYPE_NONE;
    int curr_section = 0;
    int curr_question = -1;

    char line[MAX_QUESTION_LEN];

    while (!feof(fp) && curr_section != MAX_SECTION_COUNT && curr_question != MAX_QUESTION_COUNT) {
        
        fgets(line, sizeof(line), fp);

        if (curr_char_type == CHARTYPE_NONE) { // NO SECTION FOUND SO FAR
            if (strncmp(line, DEF_SECTION, strlen(DEF_SECTION)) == 0 ) { // FOUND FIRST SECTION
                curr_char_type = CHARTYPE_SECTION;
                strncat(sections[curr_section].name, line + strlen(DEF_SECTION), MAX_SECTION_NAME_LEN);
            }

        } else if (curr_char_type == CHARTYPE_SECTION) {
            if (strncmp(line, DEF_QUESTION, strlen(DEF_QUESTION)) == 0 ) { // FOUND NEW QUESTION
                curr_char_type = CHARTYPE_QUESTION;
                curr_question++;
                strncat(sections[curr_section].questions[curr_question], line + strlen(DEF_QUESTION), MAX_QUESTION_LEN);
            } else { // SECTION NAME CONTINUES IN NEWLINE - ADD IT TO NAME STRING
                strncat(sections[curr_section].name, line, MAX_SECTION_NAME_LEN-strlen(sections[curr_section].name)-1);
            }

        } else if (curr_char_type == CHARTYPE_QUESTION) {
            if (strncmp(line, DEF_ANSWER, strlen(DEF_ANSWER)) == 0 ) { //FOUND ANSWER FOR PREVIOUS QUESTION
                curr_char_type = CHARTYPE_ANSWER;
                strncat(sections[curr_section].answers[curr_question], line + strlen(DEF_ANSWER), MAX_QUESTION_LEN);
            } else { // QUESTION GOES INTO NEW LINE
               strncat(sections[curr_section].questions[curr_question], line, MAX_QUESTION_LEN-strlen(sections[curr_section].questions[curr_question])-1);
            }

        } else if (curr_char_type == CHARTYPE_ANSWER) {
            if (strncmp(line, DEF_SECTION, strlen(DEF_SECTION)) == 0 ) { //FOUND NEW SECTION AFTER ANSWER
                curr_char_type = CHARTYPE_SECTION;
                curr_section++;
                curr_question = -1;
                strncat(sections[curr_section].name, line + strlen(DEF_SECTION), MAX_SECTION_NAME_LEN);
            } else if (strncmp(line, DEF_QUESTION, strlen(DEF_QUESTION)) == 0 ) { // FOUND NEW QUESTION AFTER ANSWER
                curr_char_type = CHARTYPE_QUESTION;
                curr_question++;
                strncat(sections[curr_section].questions[curr_question], line + strlen(DEF_QUESTION), MAX_QUESTION_LEN);
            } else { // ANSWER GOES INTO NEW LINE
                strncat(sections[curr_section].answers[curr_question], line, MAX_QUESTION_LEN-strlen(sections[curr_section].answers[curr_question])-1);
            }

        }

    }

    return 0; //TODO: fix return stuff
}

int choose_section ()
{

    printf("The following sections were found in this file:\n");

    int i;
    for (i = 0; strlen(sections[i].name) > 0; i++)
    {
        printf("%d:%s", i, sections[i].name);
    }

    printf("Choose your section: ");

    int c = getchar() - '0';

    if (c >= 0 && c < i) {
        sel_section = c;
        printf("Successfully selected section %d!\n", sel_section);
    } else {
        return 1;
    }

    return 0;
}

void choose_random_questions(int section_n) //TODO: add free memory stuff
{

    int question_n = get_question_count(section_n);

/*    int cha = 0;
    while (strlen(sections[section_n].questions[cha]) > 0) {
        printf("question string: %s\n", sections[section_n].questions[cha]);
        printf("answer string: %s\n", sections[section_n].answers[cha]);
        cha++;
    }
    printf("LEN: %d\n", strlen(DEF_QUESTION)); */

    int random_n;
    int last_question_n;
    srandom((unsigned int)time(NULL));
    while (1) {
        random_n = random() % question_n;
        if (random_n != last_question_n) {
            last_question_n = random_n;
            print_question(section_n, random_n);
            getchar();
            print_answer(section_n, random_n);
            printf("\033[0;31m"); // set color to red
            printf("Press enter to continue to new random question.");
            printf("\033[0m"); // set color back to default
            getchar();
        }
    }

}

void print_question (int section_n, int question_n) {
    if (section_n >= 0 && section_n < section_count && question_n >= 0 && question_n < get_question_count(section_n)) {
        printf("\033[2J\033[1;1H"); // clear screen
        printf("\033[0;33m"); // set color to yellow
        printf("Question: ");
        printf("\033[0m"); // set color back to default
        printf(sections[section_n].questions[question_n]);
    }
}
void print_answer (int section_n, int question_n) {
    if (section_n >= 0 && section_n < section_count && question_n >= 0 && question_n < get_question_count(section_n)) {
        printf("\033[0;33m"); // set color to yellow
        printf("Answer: ");
        printf("\033[0m"); // set color back to default
        printf(sections[section_n].answers[question_n]);
    }
}

int get_section_count () {

    int section_count;

    for (section_count = 0; strlen(sections[section_count].name) != 0; section_count++);
    return section_count;

}
int get_question_count (int section_n) {
    
    int question_count;

    for (question_count = 0; strlen(sections[section_n].questions[question_count]) != 0; question_count++);
    return question_count;

}
