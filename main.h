/*
 *   main.h: header file for vocab
 *
 *   Copyright (C) 2021 venji10 <venji10@riseup.net>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _MAIN_H_
#define _MAIN_H_

#define MAX_SECTION_COUNT 50

#define MAX_SECTION_NAME_LEN 30

#define MAX_QUESTION_LEN 1000
#define MAX_QUESTION_COUNT 1000

#define DEF_SECTION "s: "
#define DEF_QUESTION "q: "
#define DEF_ANSWER "a: "

enum char_types{CHARTYPE_NONE,
                CHARTYPE_SECTION,
                CHARTYPE_QUESTION,
                CHARTYPE_ANSWER};

struct section {
    char name[MAX_SECTION_NAME_LEN];
    char questions[MAX_QUESTION_COUNT][MAX_QUESTION_LEN];
    char answers[MAX_QUESTION_COUNT][MAX_QUESTION_LEN];
};

struct section sections[MAX_SECTION_COUNT];

int sel_section = -1;
int section_count = -1;

int init_sections(FILE *fp);
int choose_section();
void choose_random_questions(int sections);
void print_question(int section_n, int question_n);
void print_answer(int section_n, int question_n);
int get_section_count();
int get_question_count(int section_n);


#endif
