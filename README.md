## vocab - beta quality
vocab is a cmdline vocab trainer for learning many things.
# How it works
Vocab reads files with sections, questions and answers.
You can use the following statements at the beginning of a newline to define them:
+ ```s: ``` for a section
+ ```q: ``` for a question
+ ```a: ``` for an answer

You have to define a section before you can define a question.
A vocab file should look something like this:
```
s: Section 1
q: Question 1
a: Answer 1
q: Question 2
a: Answer 2
s: Section 2
q: Question 1
a: Answer 1
q: Question 2
a: Answer 2
```

You can load such a file with ```vocab <filename>```
You can also change the statements for sections, questions and answers, by changing their definitions in the ```main.h``` header

# Installation
You can compile the Program with ```make```.

To install it, you can run ```sudo make install```.
