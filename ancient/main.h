#ifndef _MAIN_H_
#define _MAIN_H_

#define MAX_SECTION_COUNT 50

#define CHAR_SECTION 's'
#define CHAR_QUESTION 'q'
#define CHAR_ANSWER 'a'

enum char_types{CHARTYPE_NONE,
                CHARTYPE_SECTION,
                CHARTYPE_QUESTION,
                CHARTYPE_ANSWER};

struct section {
    char name[20];
    char questions[1000][1000]; //HAS TO BE THE SAME SIZE ?
    char answers[1000][1000];   // SAME HERE ?
};

struct section sections[MAX_SECTION_COUNT];

int sel_section = -1;
int last_question_n = -1;

int init_sections(FILE *fp);
int choose_section();
int choose_random_question(int section);

#endif
