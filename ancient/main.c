#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"

int main (int argc, char **argv)
{

    if(argc != 2) 
    {
        printf("not exactly one argument provided!\n");
        return 1;
    }

    FILE *fp;
    if(!(fp = fopen(argv[1], "r")))
    {
        printf("fopen error: File not found / no permissions?\n");
        return 1;
    }

    init_sections(fp);
    fclose(fp);

    while(choose_section()) { //continue asking for correct section number, if previously entered number isn't a section id
        printf("Sorry, try that again!\n");
    }

    // perma loop random questions
    while (1) {
        choose_random_question(sel_section);
    }

    return 0;

}

int init_sections (FILE *fp)
{
    char c;
    int i = 0;
    int j = -1;

    int is_newline = 1;
    int curr_section_n = -1;
    int curr_char_type = CHARTYPE_NONE;

    while (c = fgetc(fp)) {

        if (feof(fp))
            break;

        if (is_newline) {
            if (fgetc(fp) == ':') {

                int i_old = i;
                int j_old = j;
                int char_type_old = curr_char_type;

                i = 0;
                int is_type_changed = 0;

                switch (c) {
                    case CHAR_SECTION:
                        if (curr_char_type == CHARTYPE_NONE ||
                            curr_char_type == CHARTYPE_ANSWER) {
                            curr_char_type = CHARTYPE_SECTION;
                            curr_section_n++;
                            j = -1;
                            is_type_changed = 1;
                        }
                        break;
                    case CHAR_QUESTION:
                        if (curr_char_type == CHARTYPE_SECTION) {
                            curr_char_type = CHARTYPE_QUESTION;
                            j++;
                            is_type_changed = 1;
                        }
                        break;
                    case CHAR_ANSWER:
                        if (curr_char_type == CHARTYPE_QUESTION) {
                            curr_char_type = CHARTYPE_ANSWER;
                            is_type_changed = 1;
                        }
                        break;
                }

                // terminate previous string
                if (is_type_changed) {
                    switch (char_type_old) { 
                        case CHARTYPE_SECTION:
                            sections[curr_section_n].name[i_old] = '\0';
                            break;
                        case CHARTYPE_QUESTION:
                            sections[curr_section_n].questions[j_old][i_old] = '\0';
                            break;
                        case CHARTYPE_ANSWER:
                            if (curr_char_type == CHARTYPE_SECTION) { 
                                sections[curr_section_n-1].answers[j_old][i_old] = '\0';
                            } else {
                                sections[curr_section_n].answers[j_old][i_old] = '\0';
                            }
                            break;
                    }
                }

                // add blank character rather than the char type char
                c = ' ';
            }
        }

        // add character to corresponding array
        if (curr_section_n != -1) {
            switch (curr_char_type) {
                case CHARTYPE_SECTION:
                    if (i != sizeof(sections[curr_section_n].name)-1) {
                        sections[curr_section_n].name[i] = c;
                        i++;
                    } else // terminate at end of array if reached
                        sections[curr_section_n].name[i] = '\0';
                    break;
                case CHARTYPE_QUESTION:
                    if (j < sizeof(sections[curr_section_n].questions)) {
                        if (i != sizeof(sections[curr_section_n].questions[j])-1) {
                            sections[curr_section_n].questions[j][i] = c;
                            i++;
                        } else {
                            sections[curr_section_n].questions[j][i] = '\0';
                        }
                    }
                    break;
                case CHARTYPE_ANSWER:
                    if (j < sizeof(sections[curr_section_n].questions)) {
                        if (i != sizeof(sections[curr_section_n].questions[j])-1) {
                            sections[curr_section_n].answers[j][i] = c;
                            i++;
                        } else {
                            sections[curr_section_n].answers[j][i] = '\0';
                        }
                    }
                    break;
            }
        }

        if (c == '\n') {
            is_newline = 1;
        } else {
            is_newline = 0;
        }

        //printf("%c", c);
    }

    return 0; //TODO: fix return stuff
}

int choose_section ()
{

    printf("The following sections were found in this file:\n");

    int i;
    for (i = 0; strcmp(sections[i].name, "") != 0; i++) //TODO: replace with strlen
    {
        printf("%d:%s", i, sections[i].name);
    }

    printf("Choose your section: ");

    int c = getchar() - '0';

    if (c >= 0 && c < i) {
        sel_section = c;
        printf("Successfully selected section %d!\n", sel_section);
    } else {
        return 1;
    }

    return 0;
}

int choose_random_question(int section_n)
{
    int i; // get amount of questions: TODO make seperate function and integer in header
    //for (i = 0; strlen(sections[section_n].questions[i]) != 0; i++);

    int cha = 0;
    printf("question string: %s\n", sections[section_n].questions[cha]);
    printf("answer string: %s\n", sections[section_n].answers[cha]);

    /*int random = rand() % i;
    printf("random: %d\n", random); */
    getchar();
    getchar();
}

int print_question () {}
int print_answer () {}
